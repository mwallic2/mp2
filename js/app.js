var imdbApp = angular.module('imdbApp', [
    'ngRoute',
    'imdbControllers'
]);

var ballparkApp = angular.module('ballparkApp', [
    'ballparkControllers'
]);

imdbApp.config(['$routeProvider',
    function($routeProvider){
        $routeProvider.
            when('/list', {
                templateUrl : './partials/list.html',
                controller : 'imdbListController'
            }).
            when('/details/:filmId', {
                templateUrl : './partials/details.html',
                controller : 'imdbDetailController'
            }).
            when('/gallery', {
                templateUrl : './partials/gallery.html',
                controller : 'imdbGalleryController'
            }).
            otherwise({
                redirectTo : '/list'
            });
    }
]);