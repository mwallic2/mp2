var imdbControllers = angular.module('imdbControllers', []);
var ballparkControllers = angular.module('ballparkControllers', []);

imdbControllers.controller('imdbListController', ['$scope', '$http', function($scope, $http) {
    $http.get('./data/imdb250.json')
        .success(function (response){
            $scope.films = response;
            angular.forEach($scope.films, function(film, index){
                film.index = index;
            });
        });
    $scope.ordering = 'title';
    $scope.descending = 'false';
}]);

imdbControllers.controller('imdbDetailController', ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http) {
    $http.get('./data/imdb250.json')
        .success(function (response){
            $scope.films = response;
            angular.forEach($scope.films, function(film, index){
                film.index = index;
            });
        });
    $scope.filmId = $routeParams.filmId;
}]);

imdbControllers.controller('imdbGalleryController', ['$scope', '$http', function($scope, $http) {
    $http.get('./data/imdb250.json')
        .success(function (response){
            $scope.films = response;
            $scope.genres = [];
            angular.forEach($scope.films, function(film, index){
                film.index = index;
                angular.forEach(film.genre, function(genre){
                    if ($scope.genres.indexOf(genre) < 0){
                        $scope.genres.push(genre);
                    }
                });
            });
            $scope.genres.sort();
            $scope.selectedGenre = "";
        });
}]);

ballparkControllers.controller('parksSliderController', ['$scope', '$http', function($scope, $http) {
    $scope.hello = 'hello';
    $http.get('./data/ballparks.json')
        .success(function (response){
            $scope.ballparks = response;
        });
}]);